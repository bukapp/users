
import { agent as _request } from "supertest"
import {get as getApplication} from '../index'
import {connection as getConnection} from '../index'
import {pool as getPool} from '../index'

export const request = _request(getApplication())

describe('Customers Endpoints', () => {

    it('GET /customers should not allow unauthentified access', async () => {
      const res = await request.get('/customers');
        expect(res.status).toEqual(403);
        expect(res.type).toEqual(expect.stringContaining('json'));
    });
    it('GET /customers/:id should not allow unauthentified access', async () => {
        const res = await request.get('/customers/11')
        expect(res.statusCode).toEqual(403)
    });
  
  });

  afterAll(done => {
    // Closing the DB connection allows Jest to exit successfully.
    getPool().pool.end()
    getConnection().close();
    done()
  })